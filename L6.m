clear all;
close all;

%% I. Wartosci zadane
% --- Pomieszczenia -----
Tpzn = -20; % st. C - nominalna temp. zewn.
Tpw1n = 20; % st. C - nom. temp. wewn. pom 1
Tpw2n = 20; % st. C - nom. temp. wewn. pom 2
Tgzn = 90;  % st. C - nom. temp. zasilania grzejnika
Tgp1n = 70; % st. C - nom. temp. powrotu grzejnika 1
Tgp2n = 70; % st. C - nom. temp. powrotu grzejnika 2
qt1n = 0;   % W - dodatkowe zrodla ciepla w pom. 1
qt2n = 0;   % W - dodatkowe zrodla ciepla w pom. 2

x1 = 10;    % m - wym. x pom. 1
y1 = 12;    % m - wym. y pom. 1
z1 = 2.2;   % m - wym. z pom. 1
% - styk sufitem -
x2 = x1;    % m - wym. x pom. 2
y2 = y1;    % m - wym. y pom. 2
z2 = z1;    % m - wym. z pom. 2

Ss = x1*y2; % m^2 - pole pow. styku
Ss1 = 2*(x1+y1)*z1+Ss;  % m^2 - pole pow. scian pom. 1
Ss2 = 2*(x2+y2)*z2+Ss;  % m^2 - pole pow. scian pom. 2

Vp1 = x1*y1*z1; % m^3 - obj. pow. w pom. 1
Vp2 = x2*y2*z2; % m^3 - obj. pow. w pom. 2
Vg1 = 0.002;    % m^3 - obj. grze. w pom. 1
Vg2 = 0.002;    % m^3 - obj. grze. w pom. 2

% --- Kociol --------
Tkpn = Tgp1n;   % st. C - nom. temp. powracajaca do kotla
Tkzn = Tgzn;    % st. C - nom. temp. wychodzaca do kotla
qkn = 10000;    % W - nom. moc kotla

Vk = Vg1;       % m^3 - obj. kotla

%% II. Fizyka
RoP = 1.2;  % kg/m^3
RoW = 1000; % kg/m^3
CpP = 2200; % W/K
CpW = 4190; % W/K

%% III. Stale pomocnicze
CvP1 = Vp1*RoP*CpP; % pojemnosc cieplna pom. 1
CvP2 = Vp2*RoP*CpP; % pojemnosc cieplna pom. 2
CvG1 = Vg1*RoW*CpW; % poj. ciep. grzejnika 1
CvG2 = Vg2*RoW*CpW; % poj. ciep. grzejnika
CvK = Vk*RoW*CpW;   % poj. ciep. kotla

% uwzglednienie scian
% CvS ~ CvG
CvP1 = CvP1+CvG1;
CvP2 = CvP2+CvG2;

%% IV. Statyka - wyznaczenie wsp.
% CvP1*Tpw1'=Kg1(Tgp1-Tpw1)-Kcw(Tpw1-Tpz)-Kp(Tpw1-Tpw2)+qt1
% CvG1*Tgp1'=CpW*fmg1(Tkz-Tgp1)-Kg1(Tgp1-Tpw1)
% CvP2*Tpw2'=Kg2(Tgp2-Tpw2)-Kcw(Tpw2-Tpz)+Kp(Tpw1-Tpw2)+qt2
% CvG2*Tgp2'=CpW*fmg2(Tkz-Tgp2)-Kg2(Tgp2-Tpw2)
% CvK*Tkz'=qk-CpW*(fmg1+fmg2)Tkz+CpW(fmg1*Tgp1+fmg2*Tgp2)

% - zapotrzebowanie na moc -
% q2 = 1.2q1
% qk = q1+q2
q1n = qkn/2.2;
q2n = 1.2*q1n;

% - przeplyw masowy -
%q1 = CpW*fmg1(Tkz-Tgp1)
%q2 = CpW*fmg2(Tkz-Tgp2)
fmg1n = q1n/CpW/(Tkzn-Tgp1n);
fmg2n = q2n/CpW/(Tkzn-Tgp2n);

% - przenikalnosc grzejnikow -
%q1 = Kg1(Tgp1-Tpw1)
%q2 = Kg2(Tgp2-Tpw2)
Kg1 = q1n/(Tgp1n-Tpw1n);
Kg2 = q2n/(Tgp2n-Tpw2n);

% - przenikalnosc scian -
Kw1 = q1n/(Tpw1n-Tpzn);
Kw2 = q2n/(Tpw2n-Tpzn);
Kp = 2*Kw2*Ss/Ss2;

spr_0 = Kg1*(Tgp1n-Tpw1n)-Kw1*(Tpw1n-Tpzn);
spr_1 = CpW*fmg1n*(Tgzn-Tgp1n)-Kg1*(Tgp1n-Tpw1n);
spr_2 = Kg2*(Tgp2n-Tpw2n)-Kw2*(Tpw2n-Tpzn);
spr_3 = CpW*fmg2n*(Tgzn-Tgp2n)-Kg2*(Tgp2n-Tpw2n);
spr_5 = qkn-CpW*((fmg1n+fmg2n)*Tkzn-fmg1n*Tgp1n-fmg2n*Tgp2n);



%% V. Dynamika - warunki poczatkowe
% Tpw1'=1/CvP1[Kg1(Tgp1-Tpw1)-Kcw(Tpw1-Tpz)-Kp(Tpw1-Tpw2)+qt1]
% Tgp1'=1.CvG1[CpW*fmg1(Tkz-Tgp1)-Kg1(Tgp1-Tpw1)]
% Tpw2'=1/CvP2[Kg2(Tgp2-Tpw2)-Kcw(Tpw2-Tpz)+Kp(Tpw1-Tpw2)+qt2]
% Tgp2'=1/CvG2[CpW*fmg2(Tkz-Tgp2)-Kg2(Tgp2-Tpw2)]
% Tkz'=1/CvK[qk-CpW*(fmg1+fmg2)Tkz+CpW(fmg1*Tgp1+fmg2*Tgp2)]

Tpz0 = Tpzn;
qk0 = qkn;
fmg10 = fmg1n;
fmg20 = fmg2n;

% zmienna pomocnicza
C1 = CpW*fmg10;  % stala przeplywowa 1
C2 = CpW*fmg20;  % stala przeplywowa 1

% x' = [Tw1' Tp1' Tw2' Tp2' Tz']^T
A_11 = -(Kg1+Kw1+Kp)/CvP1;
A_12 = Kg1/CvP1;
A_13 = Kp/CvP1;
A_14 = 0;
A_15 = 0;

A_21 = Kg1/CvG1;
A_22 = -(C1+Kg1)/CvG1;
A_23 = 0;
A_24 = 0;
A_25 = C1/CvG1;

A_31 = Kp/CvP2;
A_32 = 0;
A_33 = -(Kg2+Kw2+Kp)/CvP2;
A_34 = Kg2/CvP2;
A_35 = 0;

A_41 = 0;
A_42 = 0;
A_43 = Kg2/CvG2;
A_44 = -(C2+Kg2)/CvG2;
A_45 = C2/CvG2;

A_51 = 0;
A_52 = C1/CvK;
A_53 = 0;
A_54 = C2/CvK;
A_55 = -(C1+C2)/CvK;

A = [A_11, A_12, A_13, A_14, A_15; 
    A_21, A_22, A_23, A_24, A_25;
    A_31, A_32, A_33, A_34, A_35; 
    A_41, A_42, A_43, A_44, A_45;
    A_51, A_52, A_53, A_54, A_55];

Ao = A^-1;

spr_A = A*Ao;

B_11 = Kw1/CvP1;
B_12 = 0;

B_21 = 0;
B_22 = 0;

B_31 = Kw2/CvP2;
B_32 = 0;

B_41 = 0;
B_42 = 0;

B_51 = 0;
B_52 = 1/CvK;

B = [B_11, B_12; B_21, B_22; B_31, B_32; B_41, B_42; B_51, B_52];
U = [Tpz0;qk0];

x = -Ao*B*U;

Tpw10 = x(1);
Tgp10 = x(2);
Tpw20 = x(3);
Tgp20 = x(4);
Tkz0 = x(5);

%% VI. Dynamika - symulacja
ramp_start = qk0;
ramp_stop = 0;
dRamp = ramp_stop-ramp_start;
tRamp = 400000;   % czas rampa
nRamp = dRamp/tRamp;    % wartosc zmiany rampa

t_const = 0.1;   % procent czasu wykresu kiedy charakterystyka ma byc stala
t_sim = tRamp/(1-2*t_const);    % czas calej symulacji
t = t_sim*t_const; %czas skoku
dTpz = Tpz0;

upper_lim = max(ramp_start,ramp_stop);
low_lim = min(ramp_start,ramp_stop);
sim('S5_1');

figure(1)
subplot(3,2,1)
plot(time,PV_qk)
title('CV_{qk}')
xlabel('t, s')
ylabel('qk, W')
subplot(3,2,2)
plot(time,PV_qk)
title('CV_{qk}')
xlabel('t, s')
ylabel('qk, W')
subplot(3,2,3)
plot(time,CV_Tw1)
title('PV_{Tw_1}')
xlabel('t, s')
ylabel('Tw_1, ^oC')
subplot(3,2,5)
plot(time,CV_Tp1)
title('PV_{Tp_1}')
xlabel('t, s')
ylabel('Tp_1, ^oC')
subplot(3,2,4)
plot(time,CV_Tw2)
title('PV_{Tw_2}')
xlabel('t, s')
ylabel('Tw_2, ^oC')
subplot(3,2,6)
plot(time,CV_Tp2)
title('PV_{Tp_2}')
xlabel('t, s')
ylabel('Tp_2, ^oC')

figure(2)
subplot(2,2,1)
plot(PV_qk,CV_Tw1)
title('CV_{qk}')
xlabel('qk, W')
ylabel('Tw_1, ^oC')
subplot(2,2,2)
plot(PV_qk,CV_Tw2)
title('CV_{qk}')
xlabel('qk, W')
ylabel('Tw_2, ^oC')
subplot(2,2,3)
plot(PV_qk,CV_Tp1)
title('CV_{qk}')
xlabel('qk, W')
ylabel('Tp_1, ^oC')
subplot(2,2,4)
plot(PV_qk,CV_Tp2)
title('CV_{qk}')
xlabel('qk, W')
ylabel('Tp_2, ^oC')

figure(3)
subplot(1,2,1)
plot(time,CV_Tkz)
title('PV_{Tkz}')
xlabel('t, s')
ylabel('Tkz, ^oC')
subplot(1,2,2)
plot(PV_qk,CV_Tkz)
title('CV_{qk}')
xlabel('qk, W')
ylabel('Tkz, ^oC')
