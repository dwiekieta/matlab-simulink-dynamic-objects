clear all;
close all;

%% I. Wartosci zadane
% --- Pomieszczenia -----
Tpzn = -20; % st. C - nominalna temp. zewn.
Tpw1n = 20; % st. C - nom. temp. wewn. pom 1
Tpw2n = 20; % st. C - nom. temp. wewn. pom 2
Tgzn = 90;  % st. C - nom. temp. zasilania grzejnika
Tgp1n = 70; % st. C - nom. temp. powrotu grzejnika 1
Tgp2n = 70; % st. C - nom. temp. powrotu grzejnika 2
qt1n = 0;   % W - dodatkowe zrodla ciepla w pom. 1
qt2n = 0;   % W - dodatkowe zrodla ciepla w pom. 2

x1 = 10;    % m - wym. x pom. 1
y1 = 12;    % m - wym. y pom. 1
z1 = 2.2;   % m - wym. z pom. 1
% - styk sufitem -
x2 = x1;    % m - wym. x pom. 2
y2 = y1;    % m - wym. y pom. 2
z2 = z1;    % m - wym. z pom. 2

Ss = x1*y2; % m^2 - pole pow. styku
Ss1 = 2*(x1+y1)*z1+Ss;  % m^2 - pole pow. scian pom. 1
Ss2 = 2*(x2+y2)*z2+Ss;  % m^2 - pole pow. scian pom. 2

Vp1 = x1*y1*z1; % m^3 - obj. pow. w pom. 1
Vp2 = x2*y2*z2; % m^3 - obj. pow. w pom. 2
Vg1 = 0.002;    % m^3 - obj. grze. w pom. 1
Vg2 = 0.002;    % m^3 - obj. grze. w pom. 2

% --- Kociol --------
Tkpn = Tgp1n;   % st. C - nom. temp. powracajaca do kotla
Tkzn = Tgzn;    % st. C - nom. temp. wychodzaca do kotla
qkn = 10000;    % W - nom. moc kotla
qkmax = qkn;
qkmin = 0;

Vk = Vg1;       % m^3 - obj. kotla

%% II. Fizyka
RoP = 1.2;  % kg/m^3
RoW = 1000; % kg/m^3
CpP = 2200; % W/K
CpW = 4190; % W/K

%% III. Stale pomocnicze
CvP1 = Vp1*RoP*CpP; % pojemnosc cieplna pom. 1
CvP2 = Vp2*RoP*CpP; % pojemnosc cieplna pom. 2
CvG1 = Vg1*RoW*CpW; % poj. ciep. grzejnika 1
CvG2 = Vg2*RoW*CpW; % poj. ciep. grzejnika
CvK = Vk*RoW*CpW;   % poj. ciep. kotla

% uwzglednienie scian
% CvS ~ CvG
CvP1 = CvP1+CvG1;
CvP2 = CvP2+CvG2;

% Regulacja pogodowa - wspolczynniki
% 1. Regulowanie temperatura zaislania
a_z = (Tgzn-Tpzn)/(Tpw1n-Tpzn);
b_z = (Tgzn-Tpw1n)/(Tpw1n-Tpzn);

% 2. Regulacja temperatura powrotu
a_p = (Tgp1n-Tpzn)/(Tpw1n-Tpzn);
b_p = (Tgp1n-Tpw1n)/(Tpw1n-Tpzn);

%% IV. Statyka - wyznaczenie wsp.
% CvP1*Tpw1'=Kg1(Tgp1-Tpw1)-Kcw(Tpw1-Tpz)-Kp(Tpw1-Tpw2)+qt1
% CvG1*Tgp1'=CpW*fmg1(Tkz-Tgp1)-Kg1(Tgp1-Tpw1)
% CvP2*Tpw2'=Kg2(Tgp2-Tpw2)-Kcw(Tpw2-Tpz)+Kp(Tpw1-Tpw2)+qt2
% CvG2*Tgp2'=CpW*fmg2(Tkz-Tgp2)-Kg2(Tgp2-Tpw2)
% CvK*Tkz'=qk-CpW*(fmg1+fmg2)Tkz+CpW(fmg1*Tgp1+fmg2*Tgp2)

% - zapotrzebowanie na moc -
% q2 = 1.2q1
% qk = q1+q2
q1n = qkn/2.2;
q2n = 1.2*q1n;

% - przeplyw masowy -
%q1 = CpW*fmg1(Tkz-Tgp1)
%q2 = CpW*fmg2(Tkz-Tgp2)
fmg1n = q1n/CpW/(Tkzn-Tgp1n);
fmg2n = q2n/CpW/(Tkzn-Tgp2n);

% - przenikalnosc grzejnikow -
%q1 = Kg1(Tgp1-Tpw1)
%q2 = Kg2(Tgp2-Tpw2)
Kg1 = q1n/(Tgp1n-Tpw1n);
Kg2 = q2n/(Tgp2n-Tpw2n);

% - przenikalnosc scian -
Kw1 = q1n/(Tpw1n-Tpzn);
Kw2 = q2n/(Tpw2n-Tpzn);
Kp = 2*Kw2*Ss/Ss2;

spr_0 = Kg1*(Tgp1n-Tpw1n)-Kw1*(Tpw1n-Tpzn);
spr_1 = CpW*fmg1n*(Tgzn-Tgp1n)-Kg1*(Tgp1n-Tpw1n);
spr_2 = Kg2*(Tgp2n-Tpw2n)-Kw2*(Tpw2n-Tpzn);
spr_3 = CpW*fmg2n*(Tgzn-Tgp2n)-Kg2*(Tgp2n-Tpw2n);
spr_5 = qkn-CpW*((fmg1n+fmg2n)*Tkzn-fmg1n*Tgp1n-fmg2n*Tgp2n);



%% V. Dynamika - warunki poczatkowe
% Tpw1'=1/CvP1[Kg1(Tgp1-Tpw1)-Kcw(Tpw1-Tpz)-Kp(Tpw1-Tpw2)+qt1]
% Tgp1'=1.CvG1[CpW*fmg1(Tkz-Tgp1)-Kg1(Tgp1-Tpw1)]
% Tpw2'=1/CvP2[Kg2(Tgp2-Tpw2)-Kcw(Tpw2-Tpz)+Kp(Tpw1-Tpw2)+qt2]
% Tgp2'=1/CvG2[CpW*fmg2(Tkz-Tgp2)-Kg2(Tgp2-Tpw2)]
% Tkz'=1/CvK[qk-CpW*(fmg1+fmg2)Tkz+CpW(fmg1*Tgp1+fmg2*Tgp2)]

Tpz0 = -10;
Twew = 10;
fmg10 = fmg1n;
fmg20 = fmg2n;

% zmienna pomocnicza
C1 = CpW*fmg10;  % stala przeplywowa 1
C2 = CpW*fmg20;  % stala przeplywowa 1

% x' = [Tw1' Tp1' Tw2' Tp2' Tz']^T
A_11 = -(Kg1+Kw1+Kp)/CvP1;
A_12 = Kg1/CvP1;
A_13 = Kp/CvP1;
A_14 = 0;
A_15 = 0;
A_16 = 0;

A_21 = Kg1/CvG1;
A_22 = -(C1+Kg1)/CvG1;
A_23 = 0;
A_24 = 0;
A_25 = C1/CvG1;
A_26 = 0;

A_31 = Kp/CvP2;
A_32 = 0;
A_33 = -(Kg2+Kw2+Kp)/CvP2;
A_34 = Kg2/CvP2;
A_35 = 0;
A_36 = 0;

A_41 = 0;
A_42 = 0;
A_43 = Kg2/CvG2;
A_44 = -(C2+Kg2)/CvG2;
A_45 = C2/CvG2;
A_46 = 0;

A_51 = 0;
A_52 = C1/CvK;
A_53 = 0;
A_54 = C2/CvK;
A_55 = -(C1+C2)/CvK;
A_56 = 1/CvK;

A_61 = -1;
A_62 = 0;
A_63 = 0;
A_64 = 0;
A_65 = 0;
A_66 = 0;

A = [A_11, A_12, A_13, A_14, A_15, A_16; 
    A_21, A_22, A_23, A_24, A_25, A_26;
    A_31, A_32, A_33, A_34, A_35, A_36; 
    A_41, A_42, A_43, A_44, A_45, A_46;
    A_51, A_52, A_53, A_54, A_55, A_56;
    A_61, A_62, A_63, A_64, A_65, A_66];

Ao = A^-1;

spr_A = A*Ao;

B_11 = Kw1/CvP1;
B_12 = 0;

B_21 = 0;
B_22 = 0;

B_31 = Kw2/CvP2;
B_32 = 0;

B_41 = 0;
B_42 = 0;

B_51 = 0;
B_52 = 0;

B_61 = 0;
B_62 = 1;

B = [B_11, B_12;
    B_21, B_22;
    B_31, B_32;
    B_41, B_42;
    B_51, B_52;
    B_61, B_62];
U = [Tpz0;Twew];

x = -Ao*B*U;

Tpw10 = x(1);
Tgp10 = x(2);
Tpw20 = x(3);
Tgp20 = x(4);
Tkz0 = x(5);
qk0 = x(6);

%% VI. Dynamika - symulacja
t_const = 0.1;   % procent czasu wykresu kiedy charakterystyka ma byc stala

T = 60*20;
TDz = T;       % op. transp. z kotla do pomieszczenia 1
TDz1 = T;      % op. transp. z pom 1 do pom 2
TDp = TDz1;     % op. t. z pom 2 do mieszalnika
TDk = TDz;      % op. t. z mieszalnika do kotla

t_sim = 400*T;    % czas calej symulacji
t = t_sim*t_const; %czas skoku

k_Tpw1_qk = 3/750;
k_Tgp1_qk = 6.75/750;
k_Tkz_qk = 8.25/750;
T0_Tpw1_qk = 1200;
T0_Tgp1_qk = 1200;
T0_Tkz_qk = 0;
T_Tpw1_qk = 23008;
T_Tgp1_qk = 20801;
T_Tkz_qk = 19128;

k_Tpw1_Tpz = 1;
k_Tgp1_Tpz = 1;
T0_Tpw1_Tpz = 0;
T0_Tgp1_Tpz = 0;
T_Tpw1_Tpz = 10075;
T_Tgp1_Tpz = 22406;

dTpz = 0;%Tpz0*0.1;
dTwew = 0;%Twew*0.1;
dfmg1 = 0;
dfmg2 = 0;
dqt1 = 0;
dqt2 = 500;
dqk0 = qk0*0.15;

Ka = 1;

linia = ["k" "--k" "-.k" "r" "--r" "-.r" ".r" ".b" ".g"];

for j = 1:3
    
    switch(j)
        case 1 %ZN1_Twew
            a = k_Tpw1_qk * T / T_Tpw1_qk;
            KP = 0.9/a;
            Ti = 3*T_Tpw1_qk;
            KI = KP/Ti;
        case 2 %ZN1_Tkz
            a = k_Tkz_qk * T / T_Tkz_qk;
            KP = 0.9/a;
            Ti = 3*T_Tkz_qk;
            KI = KP/Ti;
        case 3 %ZN1_Tgp1
            a = k_Tgp1_qk * T / T_Tgp1_qk;
            KP = 0.9/a/10;
            Ti = 3*T_Tgp1_qk;
            KI = KP/Ti;            
    end
 
    

    sim('S13');
    
    lw = 0.5;
    
    figure(1)
    if j ~=4
        subplot(1,2,1)
        title('Tpw')
        xlabel('czas, s');
        ylabel('temperatura, ^oC');
        hold on;
        plot(time,PV_Tw1,linia(j),'LineWidth',lw);
        plot(time,PV_Tw2,linia(j+3),'LineWidth',lw);
        plot(time,ones(size(time))*(Twew+dTwew),'--');
        wsk = 0.01;
        plot(time,ones(size(time))*(Twew+dTwew)*(1+wsk),'blue');
        plot(time,ones(size(time))*(Twew+dTwew)*(1-wsk),'blue');
        
%         subplot(1,4,2)
%         title('Tkz')
%         xlabel('czas, s');
%         ylabel('temperatura, ^oC');
%         hold on;
%         plot(time,PV_Tkz,linia(j),'LineWidth',lw);
%         tz = PV_Tkz(length(time));
%         plot(time,ones(size(time))*tz,'--');
%         wsk = 0.01;
%         plot(time,ones(size(time))*tz*(1+wsk),'blue');
%         plot(time,ones(size(time))*tz*(1-wsk),'blue');
%         
%         subplot(1,4,3)
%         title('Tgp1')
%         xlabel('czas, s');
%         ylabel('temperatura, ^oC');
%         hold on;
%         plot(time,PV_Tgp1,linia(j),'LineWidth',lw);
%         tp = PV_Tgp1(length(time));
%         plot(time,ones(size(time))*tp,'--');
%         wsk = 0.01;
%         plot(time,ones(size(time))*tp*(1+wsk),'blue');
%         plot(time,ones(size(time))*tp*(1-wsk),'blue');
        
        subplot(1,2,2)
        title('qk')
        xlabel('czas, s');
        ylabel('moc, W');
        hold on;
        plot(time,CV_qk,linia(j),'LineWidth',lw);
        q = CV_qk(length(time));
        plot(time,ones(size(time))*q,'--');
        wsk = 0.01;
        plot(time,ones(size(time))*q*(1+wsk),'blue');
        plot(time,ones(size(time))*q*(1-wsk),'blue');
    end
    
    if j<0
        figure(2)
        if j == 1
            subplot(1,4,1)
            title('Tpw1')
            xlabel('czas, s');
            ylabel('temperatura, ^oC');
            hold on;
            plot(time,PV_Tw1_Model,linia(j+3));
            plot(time,ones(size(time))*(Twew+dTwew),'--');
            wsk = 0.01;
            plot(time,ones(size(time))*(Twew+dTwew)*(1+wsk),'blue');
            plot(time,ones(size(time))*(Twew+dTwew)*(1-wsk),'blue');
        end
        
        if j == 2
            subplot(1,4,2)
            title('Tkz')
            xlabel('czas, s');
            ylabel('temperatura, ^oC');
            hold on;
            plot(time,PV_Tkz_Model,linia(j+3));
            tz = PV_Tkz_Model(length(time));
            plot(time,ones(size(time))*tz,'--');
            wsk = 0.01;
            plot(time,ones(size(time))*tz*(1+wsk),'blue');
            plot(time,ones(size(time))*tz*(1-wsk),'blue');
        end
        
        if j == 3
            subplot(1,4,3)
            title('Tgp1')
            xlabel('czas, s');
            ylabel('temperatura, ^oC');
            hold on;
            plot(time,PV_Tgp1_Model,linia(j+3));
            tp = PV_Tgp1_Model(length(time));
            plot(time,ones(size(time))*tp,'--');
            wsk = 0.01;
            plot(time,ones(size(time))*tp*(1+wsk),'blue');
            plot(time,ones(size(time))*tp*(1-wsk),'blue');
        end
        
        subplot(1,4,4)
        title('qk')
        xlabel('czas, s');
        ylabel('moc, W');
        hold on;
        plot(time,CV_qk_Model,linia(j+3));
        q = CV_qk_Model(length(time));
        plot(time,ones(size(time))*q,'--');
        wsk = 0.01;
        plot(time,ones(size(time))*q*(1+wsk),'blue');
        plot(time,ones(size(time))*q*(1-wsk),'blue');
        
    end
           

end