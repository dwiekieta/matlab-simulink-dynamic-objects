clear all;

%% I. Wartosci zadane
% --- Pomieszczenie -----
Tpzn = -20; % nominalna temp. zewn.
Tpwn = 20;  % nom. temp. wewn.
Tgzn = 90;  % nom. temp. zasilania grzejnika
Tgpn = 70;  % nom. temp. powrotu grzejnika
qn = 2000;  % nom. zapot. na cieplo pomieszczenia

Vp = 50;    %m^3 - obj. pow.
Vg = 0.002; %m^3 - obj. grze.

% --- Kociol --------
Tkpn = Tgpn;    % nom. temp. powracajaca do kotla
qk = qn;        % nom. moc kotla

Vk = Vg;        % obj. kotla

%% II. Fizyka
RoP = 1.2;  %kg/m^3
RoW = 1000; %kg/m^3
CpP = 2200; %W/K
CpW = 4190; %W/K

%% III. Stale pomocnicze
CvP = Vp*RoP*CpP;   % pojemnosc cieplna pomieszczenia
CvG = Vg*RoW*CpW;   % poj. ciep. grzejnika
CvK = Vk*RoW*CpW;   % poj. ciep. kotla

%% IV. Statyka - wyznaczenie wsp.
% CvP*Tpw'=Kcg(Tgp-Tpw)-Kcw(Tpw-Tpz)
% CvG*Tgp'=CpW*fmg(Tkz-Tgp)-Kcg(Tgp-Tpw)
% CvK*Tkz'=qk-CpW*fmg(Tkz-Tkp)
% qn=CpW*fmg(Tgz-Tgp)=Kcg(Tgpn-Tpwn)=Kcw(Tpw-Tpz)

fgn = qn/CpW/RoW/(Tgzn-Tgpn); %m^3/s - przeplyw obj. nominalny
fgmn = RoW*fgn; %kg/s - przeplyw masowy nominalny;
Kcg = qn/(Tgpn-Tpwn); %J/K - sprawnosc oddawania ciepla przez grzejnik
Kcw = qn/(Tpwn-Tpzn); %J/K - sprawnosc oddawania ciepla przez sciany

%% V. Dynamika - warunki poczatkowe
% Tpw' = 1/CvP*(Kcg(Tgp-Tpw)-Kcw(Tpw-Tpz))
% Tgp' = 1/CvG*(CpW*fgm(Tgz-Tgp)-Kcg(Tgp-Tpw))
% Tkz'=1/CvK*(qk-CpW*fmg(Tkz-Tkp))

Tpz0 = Tpzn;
Tgz0 = Tgzn;
Tkp0 = Tkpn;
fgm0 = fgmn;

% zmienna pomocnicza
CF = CpW*fgmn;  % stala przeplywowa
M = CF*(Kcg+Kcw)+Kcg*Kcw; % mianownik

% --- wyznaczzenie punkt�w pracy ---
Tpw0 = (Tpz0*Kcw*(Kcg+CF)+CF*Tgz0*Kcg)/M;
Tgp0 = (Tpw0*Kcg+CF*Tgz0)/(CF+Kcg);
Tkz0 = (qk+CF*Tkp0)/CF;

%% VI. Dynamika - symulacja
t = 2; %czas skoku
dTpz = Tpz0;

%sim('S3_BlokPom_Kociol');