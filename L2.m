clear all;
%% Kociol
%% I. Wartosci zadane
Tgzn = 90;
Tgpn = 70;
qk = 2000; %W

Vk = 0.002; %m^3 - obj. kotla

%% II. Stale fizyczne
RoW = 1000; %kg/m^3
CpW = 4190; %W/K

%% III. Stale pomocniczne
CvK = RoW*CpW*Vk;

%% IV. Dynamika - wyznaczenie przeplywu
% CvK*Tkz'=qk-CpW*RoW*fg*(Tkz-Tkp)
Tkz0 = Tgzn;
Tkp0 = Tgpn;

fkn = qk/CpW/RoW/(Tkz0-Tkp0);
fmkn = RoW*fkn;