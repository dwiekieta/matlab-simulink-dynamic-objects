%% Pomieszczenie 
%% I. Wartosci zadane
Tpzn = -20;
Tpwn = 20;
Tgzn = 90;
Tgpn = 70;
qn = 2000; %zapot. na cieplo

Vp = 50; %m^3 - obj. pow.
Vg = 0.002; %m^3 - obj. grze.

%% II. Fizyka
RoP = 1.2; %kg/m^3
RoW = 1000; %kg/m^3
CpP = 2200; %W/K
CpW = 4190; %W/K

%% III. Stale pomocnicze
CvP = Vp*RoP*CpP;
CvG = Vg*RoW*CpW;

%% IV. Statyka - wyznaczenie wsp.
% CvP*Tpw'=Kcg(Tgp-Tpw)-Kcw(Tpw-Tpz)
% CvG*Tgp'=CpW*RoW*fg(Tgz-Tgp)-Kcg(Tgp-Tpw)
% qn=CpW*RoW*fg(Tgz-Tgp)=Kcg(Tgpn-Tpwn)=Kcw(Tpw-Tpz)

fgn = qn/CpW/RoW/(Tgzn-Tgpn); %m^3/s - przeplyw obj. nominalny
fgmn = RoW*fgn; %kg/s - przeplyw masowy nominalny;
Kcg = qn/(Tgzn-Tgpn); %J/K - sprawnosc oddawania ciepla przez grzejnik
Kcw = qn/(Tpwn-Tpzn); %J/K - sprawnosc oddawania ciepla przez sciany

%% V. Dynamika - warunki poczatkowe
% Tpw' = 1/CvP*(Kcg(Tgp-Tpw)-Kcw(Tpw-Tpz))
% Tgp' = 1/CvG*(CpW*fgm(Tgz-Tgp)-Kcg(Tgp-Tpw))

Tpz0 = Tpzn;
Tgz0 = Tgzn;
fgm0 = fgmn;

% zmienna pomocnicza
CF = CpW*fgmn;  % stala przeplywowa
M = CF*(Kcg+Kcw)+Kcg*Kcw; % mianownik

% --- wyznaczzenie punkt�w pracy ---
Tpw0 = (Tpz0*Kcw*(Kcg+CF)+CF*Tgz0*Kcg)/M;
Tgp0 = (Tpw0*Kcg+CF*Tgz0)/(CF+Kcg);

%% VI. Dynamika - symulacja
t = 2; %czas skoku
dTpz = Tpz0+10;

sim('S2_BlokPom');