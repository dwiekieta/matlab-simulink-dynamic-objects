In this repository are scripts and models referended to practical classes of identification, 
	modeling and controlling dynamic systems (heating room by radiator and boiler) at WUST
	faculty of Electronics, field of study Control Engineering and Robotics

	L1:S2 - Static models of room and bioler
	L2:S3 - Calculations operation points of boiler
	L4:S3 - Calcualtions all operation points of system
	L5:S4 - Introduces mass flow in the Central Heating System  
	L6:S5 - Ramp function in symulations
	L7:S6 - Introduces PID controllers
	L10:S9 - Introduces model of room system 